package TimeComplexity;

import java.util.Scanner;
/**
 * Created by Amin Khaki Moghadam on 10/11/2016.
 * Time Complexity Project - O(2^n)
 */
class TowersOfHanoi {
    private void solve(int n, String start, String auxiliary, String end) {
        if (n == 1) {
            System.out.println(start + " -> " + end);
        } else {
            solve(n - 1, start, end, auxiliary);
            System.out.println(start + " -> " + end);
            solve(n - 1, auxiliary, start, end);
        }
    }

    static void h2n() {
        TowersOfHanoi temp = new TowersOfHanoi();
        System.out.print("Enter number of discs: ");
        Scanner scanner = new Scanner(System.in);
        int discs = scanner.nextInt();
        long startTime = System.currentTimeMillis();
        temp.solve(discs, "A", "B", "C");
        long endTime = System.currentTimeMillis();
        System.out.println("This took " + (endTime-startTime) + " Milliseconds");
    }
}

