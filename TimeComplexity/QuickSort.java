package TimeComplexity;

import java.util.Arrays;
/**
 * Created by Amin Khaki Moghadam on 11/11/2016.
 * Time Complexity Project - O(N log n)
 */
class QuickSort extends ArrayMaker{
    private void quickHand() {
        int low = 0;
        int high = num.length - 1;
        long startTime = System.currentTimeMillis();
        quickSort(num, low, high);
        //System.out.println(Arrays.toString(num)); /* Uncomment to show the sorted numbers. */
        long endTime = System.currentTimeMillis();
        System.out.println("This took " + (endTime-startTime) + " Milliseconds");
    }
    private void quickSort(int[] arr, int low, int high) {
        if (arr == null || arr.length == 0)
            return;
        if (low >= high)
            return;
        int middle = low + (high - low) / 2;
        int pivot = arr[middle];
        int i = low, j = high;
        while (i <= j) {
            while (arr[i] < pivot) {
                i++;
            }
            while (arr[j] > pivot) {
                j--;
            }
            if (i <= j) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
        }
        if (low < j)
            quickSort(arr, low, j);

        if (high > i)
            quickSort(arr, i, high);
    }
    static void nLogN() {
        QuickSort temp = new QuickSort();
        temp.input();
        temp.quickHand();
    }
}
