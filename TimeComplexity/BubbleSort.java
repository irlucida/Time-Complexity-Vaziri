package TimeComplexity;

/**
 * Created by Amin Khaki Moghadam on 01/11/2016.
 * Time Complexity Project - O(N^2)
 */
class BubbleSort extends ArrayMaker{
    private void bSort(){
        long startTime = System.currentTimeMillis();
        for (int a = 0; a < (i-1); a++)
        {
            for (int b = 0; b < (i - a - 1); b++)
            {
                if (num[b + 1]<num[b])
                {
                    int temp;
                    //Swapping
                    temp = num[b];
                    num[b] = num[b+1];
                    num[b + 1] = temp;
                }
            }
        }
        long endTime = System.currentTimeMillis();
        System.out.println("This took " + (endTime-startTime) + " Milliseconds");
    }
    static void N82(){
        BubbleSort temp = new BubbleSort();
        temp.input();
        temp.bSort();
    }
}
